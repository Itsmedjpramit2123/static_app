export default ({ app }, inject) => {
    const getBaserUrl = () => {
        return process.env.APP_NAME   
    };  
    inject("getBaserUrl", getBaserUrl);
  }